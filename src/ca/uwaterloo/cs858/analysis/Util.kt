package ca.uwaterloo.cs858.analysis

class MattUtil {
    companion object {
        fun convertToPackageName(name: String): String {
            return name.replace("^L".toRegex(), "")
                    .replace("/", ".")
                    .replace("^(.+)\\.(.+?)$".toRegex(), "$1")
        }
    }
}