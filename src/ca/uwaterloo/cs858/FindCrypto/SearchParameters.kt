package ca.uwaterloo.cs858.FindCrypto

import com.ibm.wala.classLoader.IMethod
import com.ibm.wala.ipa.callgraph.CallGraph
import com.ibm.wala.util.graph.Graph

abstract class SearchParameters(var methods: Array<IMethod>) {
    abstract fun sourceFinder(graph: Graph<IMethod>): List<IMethod>?
    abstract fun cryptoFinder(graph: CallGraph): List<IMethod>?
    abstract fun intermediateFinder(graph: Graph<IMethod>): List<IMethod>?
    abstract fun sinkFinder(graph: Graph<IMethod>): List<IMethod>?
}